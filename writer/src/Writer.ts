import * as readline from "readline";
import * as NATS from "nats";
import fs from "fs";
import { ConfigInterface } from './config/ConfigInterface'

export class Writer {

    private config: ConfigInterface
    private nc: NATS.Client
    private filePath: string
    private buffer: Array<Buffer | string> = []
    private isWriting: boolean = false
    private endReceived: boolean = false
    private countChunks: number = 0
    private totalChunks: number = Infinity
    private writeStream: fs.WriteStream | null = null

    constructor (config: ConfigInterface, natsClient: NATS.Client ) {
        this.config = config
        this.nc = natsClient
        this.filePath = this.config.dataPath + '/bigfile.txt'
        console.log("I'm ready")
    }

    private readerResponse () {
        this.nc.subscribe('veryaskinTest_pingWriter', (msg: any, reply: any) => {
            this.nc.publish(reply, "I'm ready.")
        })
    }

    private processData (data: any) {
        return new Promise(resolve => {
            setTimeout(() => {
                resolve(data)
            }, 20)
        })
    }

    private async receiveFromReader () {
        this.nc.subscribe('veryaskinTest_chunk', async (chunk: Buffer | string) => {

            this.buffer.push(chunk)

            if (!this.writeStream) {

                console.log();
                console.log('Deleting ' + this.filePath)

                try {
                    await fs.unlinkSync(this.filePath)
                } catch (error) {}

                this.writeStream = fs.createWriteStream(this.filePath)

                console.log()
                console.log("Receiving file...")
                process.stdout.write(this.countChunks + ' chunks received');

                this.writeFile()

            } else {
                if (!this.isWriting) {
                    this.writeFile()
                }
            }
        })

        this.nc.subscribe('veryaskinTest_end', (totalChunks: string) => {
            this.endReceived = true;
            this.totalChunks = Number(totalChunks);
            this.writeFile()
        })
    }

    private async writeFile() {

        this.isWriting = true;

        while (this.buffer.length > 0) {
            const chunk = this.buffer.shift()
            // this.writeStream?.write(await this.processData(chunk))
            this.writeStream?.write(chunk)

            readline.clearLine(process.stdout, 0);
            readline.cursorTo(process.stdout, 0);
            process.stdout.write(this.countChunks + ' chunks written, ' + this.buffer.length + ' in buffer');
            this.countChunks++
        }

        this.isWriting = false;

        if (this.endReceived && this.countChunks >= this.totalChunks) {
            this.writeStream?.end()
            console.log('')
            console.log('Receiving is finished')
        }
    }

    public run (dataPath: string = this.config.dataPath) {
        this.readerResponse()
        this.receiveFromReader()
    }
}
