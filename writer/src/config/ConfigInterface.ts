export interface ConfigInterface {
    dataPath: string,
    nats: {
        host: string,
        port: number,
    }
}