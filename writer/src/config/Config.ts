import { ConfigInterface } from './ConfigInterface'

export const config: ConfigInterface = {
    dataPath: './data',
    nats: {
        host: 'http://0.0.0.0',
        port: 4222,
    }
};