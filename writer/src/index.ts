import { config } from './config/Config'
import { Writer } from './Writer'
import * as NATS from 'nats'

const natsClient = NATS.connect(config.nats.host + ':' + config.nats.port)

const writer = new Writer(config, natsClient)

writer.run()