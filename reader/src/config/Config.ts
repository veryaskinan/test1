import { ConfigInterface } from './ConfigInterface'

export const config: ConfigInterface = {
    filePath: './data/file.txt',
    nats: {
        host: 'http://0.0.0.0',
        port: 4222,
    }
};