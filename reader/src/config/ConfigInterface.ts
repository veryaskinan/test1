export interface ConfigInterface {
    filePath: string,
    nats: {
        host: string,
        port: number,
    }
}