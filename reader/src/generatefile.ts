import { config } from './config/Config'
import fs from "fs";

(async () => {
    try {
        await fs.unlinkSync(config.filePath)
    } catch (error) {}

    process.stdout.write("Creating big file ...");
    const file = fs.createWriteStream(config.filePath)
    for (let i = 0; i <= 4e5; i++) {
        await file.write('Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\n')
    }
    file.end()
    console.log('')
    console.log('Waiting file to be saved ...')
})()