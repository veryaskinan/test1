import { config } from './config/Config'
import { Reader } from './Reader'
import * as NATS from 'nats'

const natsClient = NATS.connect(config.nats.host + ':' + config.nats.port)

const reader = new Reader(config, natsClient)

reader.run()