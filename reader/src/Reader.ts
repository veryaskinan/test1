import * as readline from "readline";
import * as NATS from "nats"
import * as fs from 'fs'
import { ConfigInterface } from './config/ConfigInterface'

export class Reader {

    private config: ConfigInterface
    private nc: NATS.Client

    constructor (config: ConfigInterface, natsClient: NATS.Client ) {
        this.config = config
        this.nc = natsClient
    }

    private async waitForWriter (): Promise<void> {
        return new Promise (resolve => {
            const options = { max: 1, timeout: 5000 }
            this.nc.request(
                'veryaskinTest_pingWriter', null, options,
                async (msg: any) => {
                    if (msg instanceof NATS.NatsError && msg.code === NATS.REQ_TIMEOUT) {
                        console.log('Writer is unavailable')
                        resolve(await this.waitForWriter());
                    } else {
                        console.log('Writer is available')
                        resolve();
                    }
                }
            )
        });
    }

    async sendToWriter (): Promise<void> {
        let countChunks = 0

        console.log();
        console.log("Sending file to writer...");
        process.stdout.write(countChunks + ' chunks sent');

        const readStream = fs.createReadStream(this.config.filePath)
        for await (const chunk of readStream) {
            this.nc.publish('veryaskinTest_chunk', chunk)

            readline.clearLine(process.stdout, 0);
            readline.cursorTo(process.stdout, 0);
            process.stdout.write(countChunks + ' chunks sent');
            countChunks++
        }
        this.nc.publish('veryaskinTest_end', String(countChunks))
        console.log('')
        console.log('Sending is finished')
    }

    async run (dataPath: string = this.config.filePath) {
        await this.waitForWriter()
        await this.sendToWriter()
    }
}
